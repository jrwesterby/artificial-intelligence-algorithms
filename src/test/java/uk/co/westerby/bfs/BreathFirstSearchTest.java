package uk.co.westerby.bfs;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;

class BreathFirstSearchTest {

  @Test
  void itVisitsEveryVertexInTheCorrectOrder() {
    Vertex vertexA = new Vertex(1);
    Vertex vertexB = new Vertex(2);
    Vertex vertexC = new Vertex(3);
    Vertex vertexD = new Vertex(4);
    Vertex vertexE = new Vertex(5);
    vertexA.addNeighbourVertex(vertexB);
    vertexA.addNeighbourVertex(vertexD);
    vertexD.addNeighbourVertex(vertexC);
    vertexB.addNeighbourVertex(vertexE);
    List<Vertex> expected = Arrays.asList(vertexA, vertexB, vertexD, vertexE, vertexC);
    List<Vertex> actual = BreathFirstSearch.execute(vertexA);
    assertThat(actual, contains(expected.toArray()));
  }

}
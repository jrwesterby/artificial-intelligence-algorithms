package uk.co.westerby.bfs;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BreathFirstSearch {

  public static List<Vertex> execute(Vertex root) {
    Queue<Vertex> queue = new LinkedList<>();
    List<Vertex> result = new ArrayList<>();
    root.setVisited(true);
    queue.add(root);
    result.add(root);
    while (!queue.isEmpty()) {
      Vertex currentVertex = queue.remove();
      currentVertex.getNeighbourList().forEach(vertex -> {
        if (!vertex.isVisited()) {
          vertex.setVisited(true);
          queue.add(vertex);
          result.add(vertex);
        }
      });
    }
    return result;
  }
}
